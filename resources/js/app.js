import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment';
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import vSelect from "vue-select";
import Ads from 'vue-google-adsense'
import CoolLightBox from 'vue-cool-lightbox'
import 'vue-cool-lightbox/dist/vue-cool-lightbox.min.css'
import shareIt from 'vue-share-it';

Vue.use(shareIt);
Vue.use(require('vue-script2'));
Vue.use(CoolLightBox);

Vue.use(Ads.Adsense);
Vue.use(Ads.InArticleAdsense);
Vue.use(Ads.InFeedAdsense);

Vue.component("v-select", vSelect);
import GoogleAuth from 'vue-google-oauth2'
const gauthOption = {
    clientId: '30173417980-1mr78rb4ietprqboovvm5j9hg3265fj2.apps.googleusercontent.com',
    scope: 'profile email',
    prompt: 'select_account',
};
Vue.use(GoogleAuth, gauthOption);

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});

import App from './view/App'
import Index from './pages/index'
import Login from './pages/login'
import Register from './pages/register'
import PostAdd from './adds/post'
import View from './adds/view'
import Myads from './adds/myads'
import Feeds from './adds/feeds'
import Detail from './adds/detail'
import Edit from './adds/edit'
import Forget from './pages/forget'
import Verify from './pages/confirm'
import Reset from './pages/reset'
import Card from './adds/card-payment'
import Profile from './pages/profile'
import Search from './adds/search'
import Hotels from './hotel/index'
import Rooms from './hotel/hotelrooms'
import Room from './hotel/room'
import View_Booking from './hotel/view'
import Bookings from './hotel/bookings'
import History from './view/history'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
        },
        {
            path: '/card/payment/:id',
            name: 'card',
            component: Card,
        },
        {
            path: '/search/:id',
            name: 'search',
            component: Search,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
        },

        {
            path: '/add/post',
            name: 'postadd',
            component: PostAdd,
        },
        {
            path: '/add/view/:id',
            name: 'view',
            component: View,
        },
        {
            path: '/myads',
            name: 'myads',
            component: Myads,
        },
        {
            path: '/feeds/:id',
            name: 'feeds',
            component: Feeds,
        },
        {
            path: '/feeds/detailed/:id',
            name: 'detailed',
            component: Detail,
        },
        {
            path: '/ads/edit',
            name: 'edit',
            component: Edit,
        },
        {
            path: '/forget/password',
            name: 'forget',
            component: Forget,
        },
        {
            path: '/forget/verify',
            name: 'confirm',
            component: Verify,
        },
        {
            path: '/forget/reset',
            name: 'reset',
            component: Reset,
        },
        {
            path: '/hotels',
            name: 'hotels',
            component: Hotels,
        },
        {
            path: '/rooms/:id',
            name: 'rooms',
            component: Rooms,
        },
        {
            path: '/room/:id',
            name: 'room',
            component: Room,
        },
        {
            path: '/booking/:id',
            name: 'booking_view',
            component: View_Booking,
        },
        {
            path: '/history',
            name: 'history',
            component: History,
        },
        {
            path: '/bookings',
            name: 'bookings',
            component: Bookings,
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

