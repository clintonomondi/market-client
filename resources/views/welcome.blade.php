<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Locate, Buy & Sell  for Free</title>
    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Locate, Buy & Sell Online for free"/>
    <meta property="og:image" content="/images/logo.png"/>
    <meta property="og:description" content="Marketlinkus is your Marketplace to Locate, Buy & Sell online everything that you need for free."/>

    <!-- Favicons -->
    <link href="/images/logo.png" rel="icon">
    <link href="/images/logo.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">
    <link rel="stylesheet" href="/css/card.css">
    <link rel="stylesheet" href="/css/feed.css">
    <link rel="stylesheet" href="/css/wizard.css">
    <link rel="stylesheet" href="/css/modal.css">
    <link rel="stylesheet" href="/css/progress.css">
    <link rel="stylesheet" href="/css/slider.css">
    <link rel="stylesheet" href="/css/pricing.css">
    <link rel="stylesheet" href="/css/invoice.css">
    <link rel="stylesheet" href="/css/slider2.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;1,100;1,200&display=swap" rel="stylesheet">

    <!-- use the latest vue-select release -->
    <script src="https://unpkg.com/vue-select@latest"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">

    <link rel="stylesheet" href="/dist/css/lightbox.min.css">
    <script src="/dist/js/lightbox-plus-jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <script data-ad-client="ca-pub-2885740322818519" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
<div class="content2">
    <div id="app">
        <app></app>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</div>


{{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
{{--<!-- marketlinkus -->--}}
{{--<ins class="adsbygoogle"--}}
     {{--style="display:block"--}}
     {{--data-ad-client="ca-pub-2885740322818519"--}}
     {{--data-ad-slot="1475707279"--}}
     {{--data-ad-format="auto"--}}
     {{--data-full-width-responsive="true"></ins>--}}
{{--<script>--}}
    {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
{{--</script>--}}
{{--<!-- ======= Footer ======= -->--}}



<footer id="footer">
    <div class="g-signin2" data-onsuccess="onSignIn"></div>

    <div class="container footer-bottom clearfix">
        <div class="row justify-content-center">
            <div class="col-sm-6">
                <div class="copyright">
                    &copy; Copyright <strong><span>Marketlinkus</span></strong>. All Rights Reserved
                </div>
            </div>
            <div class="col-sm-3">
                <div class="credits">
                <div>
                    <a href="/Privacy Policy.pdf">Privacy policy</a>
                </div>
                <div>
                    <a href="/Terms of use.pdf">Terms and conditions</a>
                </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="credits">
                    <div>
                     <a href="#">About us</a>
                    </div>
                    <div>
                     <a href="#">Contact us</a>
                    </div>

                </div>
            </div>
        </div>



    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>

<!-- Vendor JS Files -->
<script src="/assets/vendor/jquery/jquery.min.js"></script>
<script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/vendor/aos/aos.js"></script>
<script src="/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="/assets/vendor/php-email-form/validate.js"></script>
<script src="/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="/assets/vendor/counterup/counterup.min.js"></script>
<script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="/assets/vendor/venobox/venobox.min.js"></script>
<script src="/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="/loader/center-loader.js"></script>
<!-- Template Main JS File -->
<script src="/assets/js/main.js"></script>
<script>
    AOS.init({
        duration: 1000,
        easing: "ease-in-out",
        once: true,
        mirror: false
    });
</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<script>
    $(document).ready(function() {
        $("#news-slider").owlCarousel({
            items : 2,
            itemsDesktop : [1199,2],
            itemsMobile : [600,1],
            pagination :true,
            autoPlay : true
        });

    });
</script>

<script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
<script src="/js/print.js"></script>
</body>

</html>
